package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class RootContextHandler extends ContextHandler {

    private final DocumentContext documentContext;

    public RootContextHandler(DocumentContext documentContext) {
        super(documentContext);
        this.documentContext = documentContext;
    }

    @Override
    ContextHandler startElement(final String localName, final Attributes atts) throws SAXException {
        if (!"testsuites".equals(localName)) {
            super.startElement(localName, atts);
        }
        return new TestSuitesContextHandler(documentContext, new ContextHandler(documentContext) {

            @Override
            ContextHandler endDocument() throws SAXException {
                return new ContextHandler(documentContext) {
                };
            }

        });
    }

}
