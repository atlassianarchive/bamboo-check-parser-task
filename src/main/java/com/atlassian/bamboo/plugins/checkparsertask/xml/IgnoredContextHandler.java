package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.SAXException;

public class IgnoredContextHandler extends ChildContextHandler {

    public IgnoredContextHandler(DocumentContext documentContext, ContextHandler parent) {
        super(documentContext, parent);
    }

    @Override
    ContextHandler characters(final char[] ch, final int start, final int length) throws SAXException {
        return this;
    }

}
