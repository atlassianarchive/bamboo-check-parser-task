package com.atlassian.bamboo.plugins.checkparsertask;

import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import org.jetbrains.annotations.NotNull;

public class CheckParserTask implements TaskType {

    private final TestCollationService testCollationService;

    public CheckParserTask(final TestCollationService testCollationService) {
        this.testCollationService = testCollationService;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {
        final ErrorMemorisingInterceptor errorLines = new ErrorMemorisingInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
        try {
            testCollationService.collateTestResults(taskContext, taskContext.getConfigurationMap().get(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN), new CheckTestReportCollector());
            return TaskResultBuilder.create(taskContext).checkTestFailures().build();
        } catch (final Exception e) {
            throw new TaskException("Failed to execute task", e);
        } finally {
            taskContext.getBuildContext().getBuildResult().addBuildErrors(errorLines.getErrorStringList());
        }
    }

}
