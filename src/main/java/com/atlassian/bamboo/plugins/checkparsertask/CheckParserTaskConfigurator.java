package com.atlassian.bamboo.plugins.checkparsertask;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.TextProvider;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class CheckParserTaskConfigurator extends AbstractTaskConfigurator implements TaskTestResultsSupport {

    private static final String DEFAULT_TEST_RESULTS_PATTERN = "**/test-reports/*.xml";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN);

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);

        return config;
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        context.put(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN, DEFAULT_TEST_RESULTS_PATTERN);
    }

    protected void validateTestResultsFilePattern(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        if (StringUtils.isEmpty(params.getString(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN))) {
            errorCollection.addError(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN, textProvider.getText("task.generic.validate.testResultsPattern.mandatory"));
        }

    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        validateTestResultsFilePattern(params, errorCollection);
    }

    @Override
    public boolean taskProducesTestResults(@NotNull final TaskDefinition taskDefinition) {
        return true;
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void setTextProvider(TextProvider textProvider) {
        this.textProvider = textProvider;
    }

}