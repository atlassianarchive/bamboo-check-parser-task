package com.atlassian.bamboo.plugins.checkparsertask.xml;

import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import org.xml.sax.Locator;
import org.xml.sax.SAXParseException;

public class DocumentContext {

    private final TestCollectionResultBuilder testCollectionResultBuilder;

    private Locator locator;

    public DocumentContext(final TestCollectionResultBuilder testCollectionResultBuilder) {
        this.testCollectionResultBuilder = testCollectionResultBuilder;
    }

    void setDocumentLocator(final Locator locator) {
        this.locator = locator;
    }

    SAXParseException parseException(final String message) {
        return new SAXParseException(message, locator);
    }

    TestCollectionResultBuilder getTestCollectionResultBuilder() {
        return testCollectionResultBuilder;
    }

}
