package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;

interface ContextHandlerFactory<P> {

    ContextHandler newContextHandler(DocumentContext documentContext, P parent, Attributes attributes);

}
