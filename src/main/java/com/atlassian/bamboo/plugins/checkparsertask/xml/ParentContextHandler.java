package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.Map;

abstract class ParentContextHandler<P extends ParentContextHandler> extends ChildContextHandler {

    private final Map<String, ContextHandlerFactory<P>> contextHandlerFactories;

    public ParentContextHandler(final DocumentContext documentContext, final ContextHandler parent, final Map<String, ContextHandlerFactory<P>> contextHandlerFactories) {
        super(documentContext, parent);
        this.contextHandlerFactories = contextHandlerFactories;
    }

    @Override
    ContextHandler startElement(final String localName, final Attributes attributes) throws SAXException {
        final ContextHandlerFactory<P> contextHandlerFactory = contextHandlerFactories.get(localName);
        if (contextHandlerFactory == null) {
            return new IgnoredContextHandler(getDocumentContext(), this);
        }
        return contextHandlerFactory.newContextHandler(getDocumentContext(), (P) this, attributes);
    }


}
