package com.atlassian.bamboo.plugins.checkparsertask;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.plugins.checkparsertask.xml.DocumentContext;
import com.atlassian.bamboo.plugins.checkparsertask.xml.PreContentHandler;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.Set;

public class CheckTestReportCollector implements TestReportCollector {

    @NotNull
    public TestCollectionResult collect(@NotNull final File file) throws Exception {
        final FileInputStream stream = new FileInputStream(file);
        try {
            final TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
            final XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            final DocumentContext documentContext = new DocumentContext(builder);
            xmlReader.setContentHandler(new PreContentHandler(documentContext));
            xmlReader.parse(new InputSource(stream));
            return builder.build();
        } finally {
            stream.close();
        }
    }

    @NotNull
    @Override
    public Set<String> getSupportedFileExtensions() {
        return Collections.emptySet();
    }

}