package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;

import java.util.HashMap;
import java.util.Map;

public class TestSuitesContextHandler extends ParentContextHandler<TestSuitesContextHandler> {

    private static final Map<String, ContextHandlerFactory<TestSuitesContextHandler>> contextHandlerFactories = new HashMap<String, ContextHandlerFactory<TestSuitesContextHandler>>();

    static {
        contextHandlerFactories.put("datetime", new ContextHandlerFactory<TestSuitesContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final TestSuitesContextHandler parent, final Attributes attributes) {
                return new IgnoredContextHandler(documentContext, parent);
            }

        });
        contextHandlerFactories.put("suite", new ContextHandlerFactory<TestSuitesContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final TestSuitesContextHandler parent, final Attributes attributes) {
                return new SuiteContextHandler(documentContext, parent);
            }

        });
    }

    public TestSuitesContextHandler(final DocumentContext documentContext, final ContextHandler parent) {
        super(documentContext, parent, contextHandlerFactories);
    }

}

