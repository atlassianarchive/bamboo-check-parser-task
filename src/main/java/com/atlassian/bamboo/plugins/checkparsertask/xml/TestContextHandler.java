package com.atlassian.bamboo.plugins.checkparsertask.xml;

import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TestContextHandler extends ParentContextHandler<TestContextHandler> {

    private static abstract class TextContextHandler extends ChildContextHandler {

        private final StringBuilder text;

        public TextContextHandler(DocumentContext documentContext, TestContextHandler parent) {
            super(documentContext, parent);
            text = new StringBuilder();
        }

        @Override
        ContextHandler characters(final char[] ch, final int start, final int length) throws SAXException {
            text.append(ch, start, length);
            return this;
        }

        @Override
        ContextHandler endElement() throws SAXException {
            setText(text.toString());
            return super.endElement();
        }

        abstract void setText(String text);

    }

    private static final Map<String, ContextHandlerFactory<TestContextHandler>> contextHandlerFactories = new HashMap<String, ContextHandlerFactory<TestContextHandler>>();

    static {
        contextHandlerFactories.put("fn", new ContextHandlerFactory<TestContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final TestContextHandler parent, final Attributes attributes) {
                return new TextContextHandler(documentContext, parent) {

                    @Override
                    void setText(final String text) {
                        parent.setFileName(text.substring(0, text.indexOf('.')));
                    }

                };
            }

        });
        contextHandlerFactories.put("id", new ContextHandlerFactory<TestContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final TestContextHandler parent, final Attributes attributes) {
                return new TextContextHandler(documentContext, parent) {

                    @Override
                    void setText(final String text) {
                        parent.setTestName(text);
                    }

                };
            }

        });
    }

    private final TestState state;

    private String fileName;

    private String testName;

    public TestContextHandler(final DocumentContext documentContext, final SuiteContextHandler parent, final Attributes attributes) {
        super(documentContext, parent, contextHandlerFactories);
        state = "success".equals(attributes.getValue("", "result")) ? TestState.SUCCESS : TestState.FAILED;
    }

    @Override
    ContextHandler endElement() throws SAXException {
        final TestResults testResults = new TestResults(fileName, testName, null);
        testResults.setState(state);
        final TestCollectionResultBuilder testCollectionResultBuilder = getDocumentContext().getTestCollectionResultBuilder();
        final Set<TestResults> testResultsSet = Collections.singleton(testResults);
        if (state == TestState.SUCCESS) {
            testCollectionResultBuilder.addSuccessfulTestResults(testResultsSet);
        } else {
            testCollectionResultBuilder.addFailedTestResults(testResultsSet);
        }
        return super.endElement();
    }

    void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    void setTestName(final String testName) {
        this.testName = testName;
    }

}
