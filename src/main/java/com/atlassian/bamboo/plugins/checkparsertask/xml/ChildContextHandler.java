package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.SAXException;

public class ChildContextHandler extends ContextHandler {

    private final ContextHandler parent;

    public ChildContextHandler(DocumentContext documentContext, ContextHandler parent) {
        super(documentContext);
        this.parent = parent;
    }

    @Override
    ContextHandler endElement() throws SAXException {
        return parent;
    }

}