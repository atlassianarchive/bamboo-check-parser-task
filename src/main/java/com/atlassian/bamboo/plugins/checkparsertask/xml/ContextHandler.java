package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public abstract class ContextHandler {

    private final DocumentContext documentContext;

    public ContextHandler(final DocumentContext documentContext) {
        this.documentContext = documentContext;
    }

    ContextHandler startDocument() throws SAXException {
        throw documentContext.parseException("Unexpected start of document");
    }

    ContextHandler endDocument() throws SAXException {
        throw documentContext.parseException("Unexpected end of document");
    }

    @SuppressWarnings({"UnusedParameters"})
    ContextHandler startElement(final String localName, final Attributes atts) throws SAXException {
        throw documentContext.parseException("Unexpected element: \"" + localName + "\"");
    }

    ContextHandler endElement() throws SAXException {
        throw documentContext.parseException("Unexpected end of element");
    }

    ContextHandler characters(final char[] ch, final int start, final int length) throws SAXException {
        for (int offset = 0; offset < length; offset++) {
            if (!Character.isWhitespace(ch[start + offset])) {
                throw documentContext.parseException("Unexpected characters: \"" + String.copyValueOf(ch, start, length) + "\"");
            }
        }
        return this;
    }

    DocumentContext getDocumentContext() {
        return documentContext;
    }

}
