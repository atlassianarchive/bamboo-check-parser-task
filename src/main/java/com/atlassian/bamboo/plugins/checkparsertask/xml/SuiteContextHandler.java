package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;

import java.util.HashMap;
import java.util.Map;

class SuiteContextHandler extends ParentContextHandler<SuiteContextHandler> {

    private static final Map<String, ContextHandlerFactory<SuiteContextHandler>> contextHandlerFactories = new HashMap<String, ContextHandlerFactory<SuiteContextHandler>>();

    static {
        contextHandlerFactories.put("title", new ContextHandlerFactory<SuiteContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final SuiteContextHandler parent, final Attributes attributes) {
                return new IgnoredContextHandler(documentContext, parent);
            }

        });
        contextHandlerFactories.put("test", new ContextHandlerFactory<SuiteContextHandler>() {

            @Override
            public ContextHandler newContextHandler(final DocumentContext documentContext, final SuiteContextHandler parent, final Attributes attributes) {
                return new TestContextHandler(documentContext, parent, attributes);
            }

        });
    }

    public SuiteContextHandler(final DocumentContext documentContext, final ContextHandler parent) {
        super(documentContext, parent, contextHandlerFactories);
    }

}
