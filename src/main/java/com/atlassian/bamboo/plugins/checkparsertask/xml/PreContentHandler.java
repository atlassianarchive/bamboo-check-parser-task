package com.atlassian.bamboo.plugins.checkparsertask.xml;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class PreContentHandler implements ContentHandler {

    private ContextHandler contextHandler;

    private final DocumentContext documentContext;

    public PreContentHandler(final DocumentContext documentContext) {
        this.documentContext = documentContext;
        contextHandler = new ContextHandler(documentContext) {

            @Override
            ContextHandler startDocument() throws SAXException {
                return new RootContextHandler(documentContext);
            }

        };
    }

    @Override
    public void setDocumentLocator(final Locator locator) {
        documentContext.setDocumentLocator(locator);
    }

    @Override
    public void startDocument() throws SAXException {
        contextHandler = contextHandler.startDocument();
    }

    @Override
    public void endDocument() throws SAXException {
        contextHandler = contextHandler.endDocument();
    }

    @Override
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {
    }

    @Override
    public void endPrefixMapping(final String prefix) throws SAXException {
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
        if (!"http://check.sourceforge.net/ns".equals(uri)) {
            throw documentContext.parseException("Unrecognised namespace: \"" + uri + "\"");
        }
        contextHandler = contextHandler.startElement(localName, atts);
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        contextHandler = contextHandler.endElement();
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {
        contextHandler = contextHandler.characters(ch, start, length);
    }

    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {
    }

    @Override
    public void processingInstruction(final String target, final String data) throws SAXException {
        throw documentContext.parseException("Unexpected processing instruction");
    }

    @Override
    public void skippedEntity(final String name) throws SAXException {
        throw documentContext.parseException("Unexpected skipped entity: \"" + name + "\"");
    }

}
